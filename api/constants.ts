import * as pkg from 'api/../package.json';

export const API_VERSION = pkg.version;
export const PORT = process.env.PORT ? parseInt(process.env.PORT, 10) : 3000;
export const PUBLIC_DIR = 'public';
