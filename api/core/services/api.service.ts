import { Injectable } from '@nestjs/common';
import { API_VERSION } from 'api/constants';

@Injectable()
export class ApiService {
  getInfo(): any {
    return { version: API_VERSION };
  }
}
