if (/^.+\.js$/.test(__filename)) {
  require('module-alias/register');
}

require('dotenv').config();

import { join } from 'path';
import { NestFactory } from '@nestjs/core';

import { PORT, PUBLIC_DIR } from 'api/constants';
import { AppModule } from 'api/app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.useStaticAssets(join(process.cwd(), PUBLIC_DIR));

  await app.listen(PORT);
}

bootstrap();
