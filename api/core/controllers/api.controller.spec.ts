import { Test, TestingModule } from '@nestjs/testing';
import { ApiController } from 'api/core/controllers/api.controller';
import { ApiService } from 'api/core/services/api.service';

describe('ApiController', () => {
  let apiController: ApiController;

  beforeAll(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [ApiController],
      providers: [ApiService],
    }).compile();

    apiController = app.get<ApiController>(ApiController);
  });

  describe('Get information', () => {
    it('should contain api version', () => {
      const { version } = apiController.info();
      const versionRegEx = /^\d+\.\d+\.\d+$/;
      expect(versionRegEx.test(version)).toBe(true);
    });
  });
});
